import kotlin.random.Random

enum class Sex { Male, Female, Other }

private val postcodeCharPool : List<Char> = listOf('A', 'B', 'D', 'E', 'F', 'G', 'H', 'J', 'L', 'N', 'P', 'Q', 'R', 'S',
        'T', 'U', 'W', 'X', 'Y', 'Z')


data class FirstName(val _name: String, private val _sex: String) {
    val name = _name.trim().trimEnd('-')
    val sex get() =
        when (_sex) {
            "M" -> Sex.Male
            "F" -> Sex.Female
            else -> Sex.Other
        }

}

data class GeographicalEntry(val _town: String, val _county: String, private val _outcode: String, val _country: String) {
    val town get() = _town.trim()
    val county get() = _county.trim()
    val postcode get() = "$_outcode ${Random.Default.nextInt(10)}${postcodeCharPool[Random.Default.nextInt(postcodeCharPool.size)]}${postcodeCharPool[Random.Default.nextInt(postcodeCharPool.size)]}"
    val country get() = _country.trim()
}

object MockUkData {

    private val lastNames: List<String> by lazy{
        this::class.java.getResource("surnames.csv")
                .readText(Charsets.UTF_8)
                .replace('\r', '\n').replace("\n\n", "\n")
                .split("\n")
                .drop(1)
                .filter { it.isNotEmpty() }
                .map {
                    it.trim().split(",")[0]
                }
    }

    private val firstNames: List<FirstName> by lazy {
        this::class.java.getResource("firstnamesuk.csv")
                .readText(Charsets.UTF_8)
                .replace('\r', '\n').replace("\n\n", "\n")
                .split("\n")
                .filter { it.isNotEmpty() }
                .map {
                    val values = it.trim().split(",")
                    FirstName(values[0], values[1])
                }
    }

    private val geographicalEntry: List<GeographicalEntry> by lazy {
        this::class.java.getResource("postcodes.csv")
                .readText(Charsets.UTF_8)
                .replace('\r', '\n').replace("\n\n", "\n")
                .split("\n")
                .drop(1)
                .filter { it.isNotEmpty() }
                .map {
                    val values = it.trim().split(",")
                    GeographicalEntry(values[5],values[6],values[0],values[9])
                }
    }

    fun getFirstName():String {
        val names: List<FirstName> = firstNames
        return names[Random.Default.nextInt(names.size)].name.titleCase()
    }

    fun getFirstNameMale(): String {
        val names: List<FirstName> = getFirstNames(Sex.Male)
        return names[Random.Default.nextInt(names.size)].name.titleCase()
    }

    fun getFirstNameFemale(): String {
        val names: List<FirstName> = getFirstNames(Sex.Female)
        return names[Random.Default.nextInt(names.size)].name.titleCase()
    }

    fun getLastName(): String {
        val names: List<String> = lastNames
        return names[Random.Default.nextInt(names.size)].titleCase()
    }

    fun getGeographicalData(): GeographicalEntry {
        val list = geographicalEntry
        return list[Random.Default.nextBits(list.size)]
    }

    fun getMobilePhoneNumber(): String {
        return when (Random.Default.nextInt(8)) {
            0 -> generatePhoneNumber("071xxxxxxxx")
            1 -> generatePhoneNumber("073xxxxxxxx")
            2 -> generatePhoneNumber("074xxxxxxxx")
            3 -> generatePhoneNumber("075xxxxxxxx")
            4 -> generatePhoneNumber("07624xxxxxx")
            5 -> generatePhoneNumber("077xxxxxxxx")
            6 -> generatePhoneNumber("078xxxxxxxx")
            7 -> generatePhoneNumber("079xxxxxxxx")
            else -> generatePhoneNumber("073xxxxxxxx")
        }
    }

    fun getWifiMobilePhoneNumber(): String {
        return when (Random.Default.nextInt(2)) {
            0 -> generatePhoneNumber("07911 2xxxxx") // wifi mobile numbers
            1 -> generatePhoneNumber("07911 8xxxxx") // wifi mobile numbers
            else -> generatePhoneNumber("07911 2xxxxx")
        }
    }

    fun getLandlineNumber(county: String): String {
        return when (county.toUpperCase()) {
            "LONDON" -> generatePhoneNumber("(020) xxxx xxxx")
            "COVENTRY" -> generatePhoneNumber("(020) xxxx xxxx")
            "CARDIFF" -> generatePhoneNumber("(029) xxxx xxxx")
            "LEEDS" -> generatePhoneNumber("(0113) xxx xxxx")
            "SHEFFIELD" -> generatePhoneNumber("(0114) xxx xxxx")
            "BIRMINGHAM" -> generatePhoneNumber("(0121) xxx xxxx")
            "EDINBURGH" -> generatePhoneNumber("(0121) xxx xxxx")
            "GLASGOW" -> generatePhoneNumber("(0141) xxx xxxx")
            "MERSEYSIDE" -> generatePhoneNumber("(0151) xxx xxxx")
            "GREATER MANCHESTER" -> generatePhoneNumber("(0161) xxx xxxx")
            "TEESIDE" -> generatePhoneNumber("(01642) xxxxxx")
            "CAMBRIDGE" -> generatePhoneNumber("(01223) xxxxxx")
            "CHORLEY" -> generatePhoneNumber("(01223) xxxxxx")
            "DUNDEE" -> generatePhoneNumber("(01382) xxxxxx")
            "EVESHAM" -> generatePhoneNumber("(01386) xxxxxx")
            "YEOVIL" -> generatePhoneNumber("(01386) xxxxxx")
            "OXFORD" -> generatePhoneNumber("(01386) xxxxxx")
            "PRESTON" -> generatePhoneNumber("(01772) xxxxxx")
            "SWANSEA" -> generatePhoneNumber("(01792) xxxxxx")
            "BOLTON" -> generatePhoneNumber("(01204) xxxxxx")
            "SEDBERGH" -> generatePhoneNumber("(0153 96) xxxxx")
            "BRAMPTON" -> generatePhoneNumber("(0169 77) 0xxxx")
            else -> generatePhoneNumber("(020) xxxx xxxx")
        }
    }

    private fun get10digitNSN(): String {
        val template = nsn10digits[Random.Default.nextInt(nsn10digits.size)]
        return generatePhoneNumber(template);
    }

    private fun get9digitNSN(): String {
        val template = nsn9digits[Random.Default.nextInt(nsn9digits.size)]
        return generatePhoneNumber(template);
    }

    private fun get7digitNSN(): String {
        val template = nsn7digits[Random.Default.nextInt(nsn7digits.size)]
        return generatePhoneNumber(template);
    }
    private fun get6digitNSN(): String {
        val template = nsn6digits[Random.Default.nextInt(nsn6digits.size)]
        return generatePhoneNumber(template);
    }


    private fun getFirstNames(s: Sex? = null): List<FirstName> {
        val names = firstNames
        return if (s == null) names else names.filter { it.sex == s }
    }
}

internal fun generatePhoneNumber(template: String): String {
    val sb = StringBuffer()
    template.forEach {
        if (it == 'x') {
            sb.append(Random.Default.nextInt(10))
        } else if (it == ' ' || it == '(' || it == ')') {
            //ignoring spaces
            return@forEach
        } else {
            sb.append(it)
        }
    }
    return sb.toString()
}

internal fun String.titleCase(): String {
    val sb = StringBuffer()
    for (idx in this.indices) {
        val c = this[idx]
        if (idx == 0) {
            sb.append(c.toUpperCase())
            continue
        } else {
            if ((idx - 1 > 0) && (this[idx-1] == '-' || this[idx-1] == '\'') || this[idx-1] == '.') {
                sb.append(c.toUpperCase())
                continue
            }
        }
        sb.append(c.toLowerCase())
    }
    return sb.toString()
}