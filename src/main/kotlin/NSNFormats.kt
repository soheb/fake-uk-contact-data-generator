val nsn10digits: List<String> = listOf(
        "01xxxxxxxxx",
        "01x1xxxxxxx",
        "011xxxxxxxx",
        "02xxxxxxxxx",
        "03xxxxxxxxx",
        "055xxxxxxxx",
        "056xxxxxxxx",
        "07xxxxxxxxx",
        "07xxxxxxxxx",
        "0800xxxxxxx",
        "08xxxxxxxxx",
        "09xxxxxxxxx"
)

val nsn9digits: List<String> = listOf(
        "(0169 77) xxxx",
        "(01xxx) xxxxx",
        "0500 xxxxxx", // withdrawn from use on 3rd June 2017
        "0800 xxxxxx"
)

val nsn7digits: List<String> = listOf(
        "0845 46 4x"
)

val nsn6digits: List<String> = listOf(
        "118xxx",
        "116xxx"
)