import spock.lang.Specification

class MockUkDataTest extends Specification {

    def "mobile phone number test"() {
        when:
        String phone = MockUkData.INSTANCE.getMobilePhoneNumber()

        then:
        phone.length() == 11
        phone.startsWith("07")
        phone.isNumber()
    }

    def "wifi mobile phone number test"() {
        when:
        String phone = MockUkData.INSTANCE.getWifiMobilePhoneNumber()

        then:
        phone.length() == 11
        phone.startsWith("07")
        phone.isNumber()
    }

    def "landline phone number test"() {
        when:
        String phone = MockUkData.INSTANCE.getLandlineNumber(county)

        then:
        phone.length() == 11
        phone.startsWith("01") || phone.startsWith("02")
        phone.isNumber()

        where:
        county              | _
        "LONDON"            | _
        "COVENTRY"          | _
        "CARDIFF"           | _
        "LEEDS"             | _
        "SHEFFIELD"         | _
        "BIRMINGHAM"        | _
        "EDINBURGH"         | _
        "GLASGOW"           | _
        "MERSEYSIDE"        | _
        "GREATER MANCHESTER"| _
        "TEESIDE"           | _
        "CAMBRIDGE"         | _
        "CHORLEY"           | _
        "DUNDEE"            | _
        "EVESHAM"           | _
        "YEOVIL"            | _
        "OXFORD"            | _
        "PRESTON"           | _
        "SWANSEA"           | _
        "BOLTON"            | _
        "SEDBERGH"          | _
        "BRAMPTON"          | _
    }

    def "name test"() {
        when:
        String name = MockUkData.INSTANCE.getFirstName()

        then:
        !name.matches(".*\\d.*")
    }
}
